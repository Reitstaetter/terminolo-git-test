#!/bin/bash

# Write log to stdout
log () {
  echo " "
  echo "##############################"
  echo "$1"
  echo "##############################"
  echo " "
}

log_simple() {
  echo " "
  echo "------> $1"
  echo " "
}

log "START OF create-terminologit.sh"

# create directory where logs from specific commands will be preserved and will be made accessible via artifacts
# these logs have to be read together with the main pipeline log
log_simple "create logging directory"
rm -rvf log_appendix
mkdir -v log_appendix

# create temporary directory for old versions of resources (content will later be copied to IG's output directory)
log_simple "create directory for old versions"
rm -rvf old
mkdir -v old

# checkout and pull latest changes to this repository
git log -n1 --format=format:"%H"
git checkout ${CI_COMMIT_BRANCH}
git pull
git log -n1 --format=format:"%H"

# retrieve template for IG
log_simple "clone terminologit-template"
git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/elga-gmbh/terminologit-template.git ./input/terminologit-template

# for integration tests (triggered by another pipeline) no IGVer will be executed
if [[ $CI_PIPELINE_SOURCE != "pipeline" ]]
then
  # retrieve old IG's output (necessary for detecting resource changes)
  log_simple "clone ${TERMGIT_HTML_PROJECT} containing old IG's output"
  git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/${TERMGIT_HTML_PROJECT}.git ./termgit-html-project

  log "RUN IGVer"

  # copy IGVer.py along with required resources to termgit-html-project/output where it will detect if resources have been changed
  log_simple "copy IGVer (+ scripts) and file containing names of changed resources to old IG's output"
  cp -v ./IGVer.py ./termgit-html-project/output/
  cp -v ./file_name_and_extension_extractor.py ./termgit-html-project/output/
  cp -v ./processedTerminologies.csv ./termgit-html-project/output/
  cp -v ./lastSuccessfulPipeline.txt ./termgit-html-project/output/

  # change into directory of old IG output
  log_simple "change to old IG's output directory"
  echo pwd: $PWD
  cd termgit-html-project/output
  echo pwd: $PWD

  # execute IGVer for detecting changed resources
  log_simple "run IGVer"
  python IGVer.py
  log_simple "finished IGVer"

  # change into root directory
  log_simple "change into root directory"
  echo pwd: $PWD
  cd ../..
  echo pwd: $PWD

  # remove all content from termgit-html-project
  log_simple "Remove old content from termgit-html-project"
  echo log_appendix/delete_termgit-html-project.log
  rm -rvf ./termgit-html-project > log_appendix/delete_termgit-html-project.log

  log "IGVer FINISHED"
else
  log "Prepare includes for integration test"
  # create list of terminologies within "terminologies/CodeSystem*" and "terminologies/ValueSet*" directories
  find terminologies/CodeSystem* terminologies/ValueSet* -type f > resource_file_path_list.txt

  echo log_appendix/prepare_includes_integration_test.log
  python prepare_includes_integration_test.py > log_appendix/prepare_includes_integration_test.log

  echo log_appendix/git_status_integration_test.log
  git status > log_appendix/git_status_integration_test.log
  log "Prepare includes for integration test finished"
fi

log "CREATE IG"

# reset folder for resources of IG-Pub and copy all .4.fhir.json files in all subdirectories
log_simple "copy .4.fhir.json files to ./input/resources"
rm -rf './input/resources'
mkdir -vp './input/resources'
echo log_appendix/copy_4.fhir.json_to_resources.log
find ./ -name '*.4.fhir.json' -exec cp -prv '{}' './input/resources' ';' > log_appendix/copy_4.fhir.json_to_resources.log

# Remove '.4.fhir' from file names in order to make them detectable by IG publisher
log_simple "remove '.4.fhir' from file names"
echo log_appendix/rename_4.fhir.json_to_json.log
find ./input/resources -type f -name '*.4.fhir.json' | while read FILE ; do
    newfile="$(echo ${FILE} |sed -e 's/.4.fhir//')" ;
    mv -v "${FILE}" "${newfile}" >> log_appendix/rename_4.fhir.json_to_json.log ;
done

# set canoncial for Implementation Guide resource
log_simple "set canoncial to: $TERMGIT_CANONICAL"
sed -i "s|TERMGIT_CANONICAL|$TERMGIT_CANONICAL|" sushi-config.yaml
log_simple "canonical has been set"

# run sushi for creating ImplementationGuid resource
log_simple "run sushi"
sushi
log_simple "sushi finished"

# download IG publisher
log_simple "download IG publisher"
curl -L https://github.com/HL7/fhir-ig-publisher/releases/latest/download/publisher.jar -o "$PWD/input-cache/publisher.jar" --create-dirs
log_simple "IG publisher downloaded"

# run IG publisher:
log_simple "run IG publisher"
unset DISPLAY
# IG publisher will be executed. If executtion of IG publisher fails, this script will fail and, thus, the pipeline as well.
./_genonce.sh || exit 1
log_simple "IG publisher finished"

# copy content from "./old" (contains all old versions of resources) to "./output"
log_simple "copy old versions to output directory"
echo log_appendix/copy_old_to_output.log
cp -v ./old/* ./output/ > log_appendix/copy_old_to_output.log

# create sitemap
log_simple "create and copy information about sitemap to output directory"
python sitemap_generator.py
cp -v ./input/sitemap/* ./output/

log "IG CREATED"

# for integration tests (triggered by another pipeline) no input/include will not be updated
if [[ $CI_PIPELINE_SOURCE != "pipeline" ]]
then
  log "UPDATE input/includes"

  # commit and push changes regarding "./input/includes" and "./input/sitemap" directory
  gitAddString="./input/sitemap ./input/includes/*-download.xhtml $(cat new_previous_versions_files.txt)"
  gitCommitMessage="${AUTOMATIC_COMMIT_PREFIX} update sitemap, downloads"
  # if the job is executed in default branch add *-previous-versions.xhtml as well:
  if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]
  then
    gitAddString="${gitAddString} ./input/includes/*-previous-versions.xhtml"
    gitCommitMessage="${gitCommitMessage}, and previous versions"
  fi
  git add $gitAddString
  git status
  git commit -m "${gitCommitMessage}"
  git push "https://${GITLAB_USER_LOGIN}:${GITLAB_CI_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git" HEAD:${CI_COMMIT_BRANCH}

  log "FINISHED UPDATE input/includes"
else
  log "input/includes not updated: pipeline source = ${CI_PIPELINE_SOURCE}"
fi

# if on default branch the changes will also be propagated to HTML project
# for integration tests (triggered by another pipeline) no HTML project will not be updated
# if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]] && [[ $CI_PIPELINE_SOURCE != "pipeline" ]]
# then
#   log "UPDATE ${TERMGIT_HTML_PROJECT}"

#   # copy current IG output to termgit-html-project/output in order to persist IG output
#   log_simple "Copy output to termgit-html-project/output"
#   echo log_appendix/copy_output_to_termgit-html-project_output.log
#   cp -rv ./output/* ./termgit-html-project/output/ > log_appendix/copy_output_to_termgit-html-project_output.log

#   # change into directory of old IG output
#   log_simple "change to termgit-html-project"
#   echo pwd: $PWD
#   cd termgit-html-project
#   echo pwd: $PWD

#   # commit and push changes
#   git add -A
#   git commit -m "${AUTOMATIC_COMMIT_PREFIX} update IG output" -m "$CI_COMMIT_MESSAGE"
#   git push "https://${GITLAB_USER_LOGIN}:${GITLAB_CI_TOKEN}@gitlab.com/${TERMGIT_HTML_PROJECT}.git" HEAD:${TERMGIT_HTML_PROJECT_DEFAULT_BRANCH}

#   log "FINISHED UPDATE ${TERMGIT_HTML_PROJECT}"
# else
#   log "${TERMGIT_HTML_PROJECT} has not been updated: commit branch = ${CI_COMMIT_BRANCH} | pipeline source = ${CI_PIPELINE_SOURCE}"
# fi

log "END OF create-terminologit.sh"
